const express = require("express");
const { getAmountV1, getAmountV2 } = require("./versions");

const app = express();

app.get("/", async (req, res) => {
  let response;
  let statusCode = 200;

  try {
    if (!req.query.amount || !req.query.transactionType) {
      statusCode = 400;
      response = "requests must contain both an amount and transaction type";
    } else {
      let parsedAmount = parseInt(req.query.amount);
      if (isNaN(parsedAmount) || parsedAmount <= 0) {
        statusCode = 400;
        response = "requests must contain an amount greater than zero";
      } else {
        if (req.query.version === "v2") {
          response = await getAmountV2(parsedAmount, req.query.transactionType);
        } else {
          response = await getAmountV1(parsedAmount, req.query.transactionType);
        }
        if (response.error != undefined) {
          statusCode = 500;
        }
      }
    }
  } catch (e) {
    statusCode = 500;
    response = `we have encountered an error: ${e}`;
  } finally {
    // send the response
    res.status(statusCode).send(response);
  }
});

module.exports = app;
