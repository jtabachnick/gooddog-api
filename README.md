# gooddog-api

## To Run
```
    npm install
    npm start
```

## Sample Request
```
http://localhost:8080?version=v1&amount=10000&transactionType=Deposit
http://localhost:8080?version=v2&amount=10000&transactionType=Deposit
```

## Notes on Changes from V1 to V2
In Version 1 there is not too much logic needed as there are only 4 correct paths to deliveing a fee. If there were more than this I might have looked to create something more like what I was doing in V2, but this seemed simple enough to just use a few short circuiting `if` statements.

In Version 2 I opted initially to use a long string of  `if-else` statements, but decided against that as it didn't seem very maintainable if more tiers were added or if they became more complex. I went with a map of values, but they could easily changed out for functions in the case of percentage based fees on categories. Allowing for easily changed out fees will allow to update more easily in future versions with minimal changes to the business logic. 


