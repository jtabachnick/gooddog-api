  
const request = require("supertest");
const app = require("../app");
const { getAmountV1, getAmountV2 } = require('../versions');

jest.mock('../versions')

describe("Test app", () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    test("bad get request no params", done => {
        request(app)
            .get('')
            .then(response => {
                expect(response.statusCode).toBe(400);
                expect(response.text).toEqual("requests must contain both an amount and transaction type")
                done();
            });
    });

    test("bad get request bad params", done => {
        request(app)
            .get('?amount=b&transactionType=blah')
            .then(response => {
                expect(response.statusCode).toBe(400);
                expect(response.text).toEqual("requests must contain an amount greater than zero")
                done();
            });
    });

    test("good get request v1", done => {
        getAmountV1.mockReturnValueOnce({
            fee: 1000,
        })
        
        request(app)
            .get('?amount=1000&transactionType=blah&version=v1')
            .then(response => {
                expect(response.statusCode).toBe(200);
                expect(response.text).toEqual("{\"fee\":1000}")
                done();
            });
    });

    test("error get request v1 error", done => {
        getAmountV1.mockReturnValueOnce({
            error: "failed",
        })
        
        request(app)
            .get('?amount=1000&transactionType=blah&version=v1')
            .then(response => {
                expect(response.statusCode).toBe(500);
                expect(response.text).toEqual("{\"error\":\"failed\"}")
                done();
            });
    });

    test("good get request v2", done => {
        getAmountV2.mockReturnValueOnce({
            platformFee: 1000,
            processingFee: 20
        })
        
        request(app)
            .get('?amount=1000&transactionType=blah&version=v2')
            .then(response => {
                expect(response.statusCode).toBe(200);
                expect(response.text).toEqual("{\"platformFee\":1000,\"processingFee\":20}")
                done();
            });
    });

    test("good get request v2", done => {
        getAmountV2.mockRejectedValue(new Error("test"))
        
        request(app)
            .get('?amount=1000&transactionType=blah&version=v2')
            .then(response => {
                expect(response.statusCode).toBe(500);
                expect(response.text).toEqual("we have encountered an error: Error: test")
                done();
            });
    });
});