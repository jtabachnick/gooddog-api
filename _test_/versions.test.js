const { getAmountV1, getAmountV2 } = require("../versions");

describe("test version 1", () => {
  test("bad request", () => {
    const expected = { error: "could not determine fee" };
    expect(getAmountV1(2001, "blah")).resolves.toEqual(expected);
  });

  test("min request, 1%", () => {
    const expected = { fee: 5};
    expect(getAmountV1(500, "blah")).resolves.toEqual(expected);
  });

  test("max request, $350", () => {
    const expected = { fee: 35000};
    expect(getAmountV1(500000, "blah")).resolves.toEqual(expected);
  });

  test("deposit request", () => {
    const expected = { fee: 2500};
    expect(getAmountV1(50000, "Deposit")).resolves.toEqual(expected);
  });

  test("full payment request", () => {
    const expected = { fee: 5000};
    expect(getAmountV1(50000, "Full Payment")).resolves.toEqual(expected);
  });
});

describe("test version 2", () => {
  test("min payment", () => {
    const expected = { platformFee: 500, processingFee: 0 };
    expect(getAmountV2(0, "blah")).resolves.toEqual(expected);
  });

  test("$10 payment", () => {
    const expected = { platformFee: 500, processingFee: 30 };
    expect(getAmountV2(1000, "blah")).resolves.toEqual(expected);
  });

  test("$100.01 payment", () => {
    const expected = { platformFee: 1000, processingFee: 300 };
    expect(getAmountV2(10001, "blah")).resolves.toEqual(expected);
  });

  test("$300.01 payment", () => {
    const expected = { platformFee: 2000, processingFee: 900 };
    expect(getAmountV2(30001, "blah")).resolves.toEqual(expected);
  });

  test("$600.01 payment", () => {
    const expected = { platformFee: 4000, processingFee: 1800 };
    expect(getAmountV2(60001, "blah")).resolves.toEqual(expected);
  });

  test("$600.01 payment", () => {
    const expected = { platformFee: 4000, processingFee: 1800 };
    expect(getAmountV2(60001, "blah")).resolves.toEqual(expected);
  });

  test("$1000.01 payment", () => {
    const expected = { platformFee: 8000, processingFee: 3000 };
    expect(getAmountV2(100001, "blah")).resolves.toEqual(expected);
  });
  
  test("$1500.01 payment", () => {
    const expected = { platformFee: 10000, processingFee: 4500 };
    expect(getAmountV2(150001, "blah")).resolves.toEqual(expected);
  });
  
  test("$2000.01 payment", () => {
    const expected = { platformFee: 12500, processingFee: 6000 };
    expect(getAmountV2(200001, "blah")).resolves.toEqual(expected);
  });
  
  test("$2500.01 payment", () => {
    const expected = { platformFee: 15000, processingFee: 7500 };
    expect(getAmountV2(250001, "blah")).resolves.toEqual(expected);
  });
  
  test("$3000.01 payment", () => {
    const expected = { platformFee: 17500, processingFee: 9000 };
    expect(getAmountV2(300001, "blah")).resolves.toEqual(expected);
  });
  
  test("$3500.01 payment", () => {
    const expected = { platformFee: 20000, processingFee: 10500 };
    expect(getAmountV2(350001, "blah")).resolves.toEqual(expected);
  });
  
  test("$10000 payment", () => {
    const expected = { platformFee: 20000, processingFee: 30000 };
    expect(getAmountV2(1000001, "blah")).resolves.toEqual(expected);
  });
});
