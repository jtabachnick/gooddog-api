const getAmountV1 = async (amount, transactionType) => {
  if (amount <= 1000) {
    return { fee: amount * 0.01 };
  }

  if (amount >= 350000) {
    return { fee: 35000 };
  }

  if (transactionType.toLowerCase() === "deposit") {
    return { fee: Math.floor(amount * 0.05 )};
  }

  if (transactionType.toLowerCase() === "full payment") {
    return { fee:Math.floor( amount * 0.1) };
  }

  return {error: "could not determine fee"};
};


// version two of fee structures
const getAmountV2 = async (amount) => {
  let platformFee = 0,
    processingFee = 0;

  processingFee = Math.floor(amount * 0.03);

    // sorting to ensure that we loop through the keys in order
    let rules = Object.keys(v2AmountRules).sort(compare);

    rules.some( r => {
        if(amount <= r){
             platformFee = v2AmountRules[r];
             return;
        }
    })

    if (amount > maxTierv2){
        platformFee = maxFeeV2;
    }

    return {
        platformFee,
        processingFee,
    }
}

// rules for amount in version 2 
// this is done for extensibility. I can add and subtract tiers wihtout changing code
// this could be further extended if there were functions that were needed
// for each tier. E.g. if at $150 we wanted to charge a percent and flat fee we
// could easily modify the code slightly to use funcitons for each tier.
// Some of these tiers could be condensed becuase they use the same percentage of the max
// this would require a function and some extra logic. 
const v2AmountRules = {
    10000: 500,
    30000: 1000, 
    60000: 2000,
    100000: 4000,
    150000: 8000,
    200000: 10000,
    250000: 12500,
    300000: 15000,
    350000: 17500
}


const maxFeeV2 = 20000;
const maxTierv2 = 350000

// compare function for sorting keys of the above map
const compare = (a,b) => {
    if( parseInt(a) < parseInt(b)){
        return -1;
    }
    if( parseInt(a) > parseInt(b)){
        return -1;
    }

    return 0;
}

exports.getAmountV1 = getAmountV1;
exports.getAmountV2 = getAmountV2;
